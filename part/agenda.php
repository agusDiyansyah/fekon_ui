<!-- Agenda -->
<div class="judul">Agenda terdekat</div>
<div class="col-md-12 agenda">
	<?php  
	for ($i=0; $i < 3; $i++) { 
	?>
	<div class="row">
		<div class="row">
			<div class="col-xs-1 col-lg-2 col-md-2 col-sm-1 omega"><span class="glyphicon glyphicon-tasks"></span></div>
			<div class="col-xs-11 col-lg-10 col-md-10 col-sm-11 alpha">
				<a href="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-1 col-lg-2 col-md-2 col-sm-1 omega"><span class="glyphicon glyphicon-time"></span></div>
			<div class="col-xs-11 col-lg-10 col-md-10 col-sm-11 alpha">
				<tgl>22 Januari 2015</tgl>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-1 col-lg-2 col-md-2 col-sm-1 omega"><span class="glyphicon glyphicon-map-marker"></span></div>
			<div class="col-xs-11 col-lg-10 col-md-10 col-sm-11 alpha">
				Gedung auditorium Universitas Tanjungpura Pontianak
			</div>
		</div>
		<div class="garis"></div>
	</div>
	<?php
	}
	?>
</div>
<div class="side-link"><a href="">Agenda lainnya</a></div>