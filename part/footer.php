<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
	<judul>Dokumen yang harus anda miliki</judul>
	<a href="">Kerangka acuan penulisan skripsi</a>
	<a href="">Kerangka acuan penulisan thesis</a>
	<a href="">Buku pedoman 2014-2015</a>
	<a href="">Alur proses pendaftaran</a>
	<a href="">Kerangka acuan penulisan karya ilmiah</a>
	<a href="">Kerangka acuan penulisan proposal bisnis</a>
	<a href="">Kerangka acuan pembuatan jurnal</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
	<judul>Profil fakultas</judul>
	<a href="">Overview</a>
	<a href="">Visi dan Misi</a>
	<a href="">Sambutan dekan</a>
	<a href="">Struktur organisasi</a>
	<a href="">Jaringan internasional</a>
	<a href="">Fasilitas</a>
	<a href="">Lokasi dan kontak</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
	<judul>Program pendidikan</judul>
	<a href="">Overview</a>
	<a href="">Visi dan Misi</a>
	<a href="">Sambutan dekan</a>
	<a href="">Struktur organisasi</a>
	<a href="">Jaringan internasional</a>
	<a href="">Fasilitas</a>
	<a href="">Lokasi dan kontak</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
	<judul>Stakeholder</judul>
	<a href="">Dosen</a>
	<a href="">Mahasiswa</a>
	<a href="">Staf administrasi</a>
	<a href="">Alumni</a>
	<a href="">Kesempatan kerja dan kemitraan</a>
</div>