<!-- Galeri -->
<div class="galeri">
	<div class="judul">Galeri</div>
	<ul class="rslides">
		<li>
			<img src="inventory/slider/demo/images/1.jpg" alt="">
			<h4>
				Lorem ipsum dolor sit amet.
			</h4>
			<footer>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
			</footer>
		</li>
		<li>
			<img src="inventory/slider/demo/images/2.jpg" alt="">
			<h4>
				Sed ut perspiciatis unde omnis.
			</h4>
			<footer>
				Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
			</footer>
		</li>
		<li>
			<img src="inventory/slider/demo/images/3.jpg" alt="">
			<h4>
				Li Europan lingues es membres del sam familie.
			</h4>
			<footer>
				Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular.
			</footer>
		</li>
	</ul>
	<br>
	<div class="side-link"><a href="">Galeri lainnya</a></div>
</div>