<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">FEKON</a>
			</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="index.php">Beranda</a></li>
				<li><a href="index.php?page=daftar">Pendaftaran</a></li>
				<li><a href="index.php?page=berita">Blog</a></li>
				<li><a href="index.php?page=kontak">Kontak</a></li>
				<li><a href="index.php?page=galeri">Galeri</a></li>
				<li><a href="#" class="bor">Border</a></li>
				<li class="visible-lg"><a href="" >large</a></li>
				<li class="visible-md"><a href="" >medium</a></li>
				<li class="visible-sm"><a href="" >small</a></li>
				<li class="visible-xs"><a href="" >xtra small</a></li>
			</ul>
			<form class="navbar-form navbar-right" role="search">
				<input class="form-control" type="email" placeholder="">
				<button class="btn btn-default">
					<span class="glyphicon glyphicon-search"></span>
				</button>
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="">f</a></li>
				<li><a href="">t</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>