<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name='description' content="">
	<meta name='viewport' content="width=device-width, initial-scale=1">
	<title>fekon ui</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="inventory/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="inventory/slider/responsiveslides.css">
	<link rel="stylesheet" type="text/css" href="inventory/style.css">
</head>
<body>
	<!-- nav -->
	<?php include 'part/nav.php'; ?>
	<!-- end nav -->
	<div class="container">
		<div class="row">
			<img src="inventory/logo.png" alt="" class="col-md-2 col-lg-2 col-sm-4 col-xs-6">
		</div>
		<br>
		<?php 
			if (!isset($_GET['page'])) {
				include 'part/baner.php'; 
			}elseif ($_GET['page']=='kontak') {
				include 'part/maps.php';
			}
		?>
		<div class="row">
			<div class="col-md-8 col-lg-8">
				<?php
					if (!isset($_GET['page'])) {
					 	include 'page/beranda.php';
					}elseif ($_GET['page']=='berita') {
						include 'page/berita.php';
					}elseif ($_GET['page']=='daftar') {
						include 'page/pendaftaran.php';
					}elseif ($_GET['page']=='kontak') {
						include 'page/kontak.php';
					}elseif ($_GET['page']=='galeri') {
						include 'page/galeri.php';
					} 
				?>
			</div>
			<div class="col-md-4 col-lg-4 side">
				<?php 
					if (!isset($_GET['page'])) {
						include 'part/kampus.php';
						include 'part/agenda.php';
						include 'part/galeri.php';
					}elseif ($_GET['page']=='berita') {
						include 'page/kontent.php';
						include 'part/kampus.php';
						include 'part/agenda.php';
						include 'part/galeri.php';
					}
					elseif ($_GET['page']=='daftar') {
						include 'part/kampus.php';
						include 'part/agenda.php';
						include 'part/galeri.php';
					}elseif ($_GET['page']=='kontak') {
						include 'page/kontakinfo.php';
					}elseif ($_GET['page']=='galeri') {
						include 'part/foto.php';
						include 'part/agenda.php';
					}
				?>
			</div>
		</div>
		<br>
		<div class="row footer">
			<?php include 'part/footer.php'; ?>
		</div>
		<br>
		<br>
	</div>
	<!-- js -->
	<script type="text/javascript" src="inventory/js/jquery.js"></script>
	<script type="text/javascript" src="inventory/slider/responsiveslides.min.js"></script>
	<script type="text/javascript" src="inventory/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0sNFo0afL4WJgJq9YF71SDNqd622KlOo"></script>
	<script type="text/javascript">
		$(".rslides").responsiveSlides({
			//timeout: 1000,
		});
		$(document).ready(function() {
			$(".bor").click(function() {
				$("div").toggleClass('border');
			});
			// maps
			function initialize() {
				var myLatlng = new google.maps.LatLng(-0.0614843, 109.3433189);
				var mapOptions = {
					zoom: 18,
					center: myLatlng
				}
				var map = new google.maps.Map(document.getElementById('map'), mapOptions);
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,  
					draggable:false,
					title: 'Universitas Muhammadiyah Pontianak'
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);
		});
	</script>
</body>
</html>