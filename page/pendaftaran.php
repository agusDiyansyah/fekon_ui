<h3>FORM PENDAFTARAN</h3>
<p>Pendaftaran Mahasiswa Tahun Ajaran 2014 - 2015 Telah Ditutup</p>
<p>
	Pendaftaran mahasiswa baru 2014/2015 <br> Program Doktor Fakultas Ekonomi - Universitas Tanjungpura
</p>
<br>
<form role="form">
	<b>A. DATA PRIBADI</b>
	<br><br>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="nama">Nama lengkap dan gelar*</label>
			<input type="text" class="form-control" id="nama">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="ttl">Tempat dan tanggal lahir*</label>
			<input type="text" class="form-control" id="ttl">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="jeniskelamin">Jenis kelamin*</label>
			<input type="text" class="form-control" id="jeniskelamin">
		</div>
		<div class="form-group col-lg-6">
			<label for="darah">Golongan darah*</label>
			<input type="text" class="form-control" id="darah">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="agama">Agama*</label>
			<input type="text" class="form-control" id="agama">
		</div>
		<div class="form-group col-lg-6">
			<label for="statuskawin">Status perkawinan*</label>
			<input type="text" class="form-control" id="statuskawin">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-12">
			<label for="alamat">Alamat rumah*</label>
			<textarea class="form-control" id="alamat"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="kota">Kota*</label>
			<input type="text" class="form-control" id="kota">
		</div>
		<div class="form-group col-lg-6">
			<label for="pos">Kode pos*</label>
			<input type="text" class="form-control" id="pos">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="telp">Telpon/No.Hp*</label>
			<input type="text" class="form-control" id="telp">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="email">Email (berkas pendaftaran akan dikirim ke email tersebut)*</label>
			<input type="text" class="form-control" id="email">
		</div>
	</div>
	<br>
	<br>
	<b>B. PROGRAM YANG DIMINATI</b>
	<br><br>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="ppilihan">Program Studi Pilihan*</label>
			<input type="text" class="form-control" id="ppilihan">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="ppilihan">Sumber biaya*</label>
			<br>
			<label>
				<input type="radio" name="biaya"> Sendiri
			</label>
			<br>
			<label>
				<input type="radio" id="instansi" name="biaya"> Instansi 
				<input type="text" class="form-control col-lg-12" id='instansi' placeholder="Nama Lembaga/Instansi">
			</label>
			<br>
		</div>
	</div>
	<br>
	<br>
	<b>C. PEKERJAAN</b>
	<br><br>
	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<div class="form-group col-lg-6">
					<label>Jenis pekerjaan*</label>
					<input type="text" class="form-control">
				</div>
				<div class="form-group col-lg-6">
					<label>Nama Instansi/Perusahaan</label>
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-lg-6">
					<label>NIP / NIS</label>
					<input type="text" class="form-control">
				</div>
				<div class="form-group col-lg-6">
					<label>Jabatan/Pangkat Golongan</label>
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-12">
			<label>Alamat Kantor</label>
			<textarea class="form-control"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>Kota</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>Kode pos kantor</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>Telepon Kantor</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<br>
	<br>
	<b>D. PENDIDIKAN SARJANA (S-1)</b>
	<br><br>
	<div class="row">
		<div class="form-group col-lg-6">
			<label>Nama perguruan tinggi*</label>
			<input type="text" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label>program studi/fakultas*</label>
			<input type="text" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-12">
			<label>Alamat lengkap perguruan tinggi</label>
			<textarea class="form-control"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>Tahun masuk*</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>Tahun lulus*</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>IPK*</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>IPK ujian negara (jika ada)</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>Status perguruan tinggi*</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>Gelar</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<br>
	<br>
	<b>E. PENDIDIKAN PASCASARJANA (S-2)</b>
	<br><br>
	<div class="row">
		<div class="form-group col-lg-6">
			<label>Nama perguruan tinggi*</label>
			<input type="text" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label>program studi/fakultas*</label>
			<input type="text" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-12">
			<label>Alamat lengkap perguruan tinggi</label>
			<textarea class="form-control"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>Tahun masuk*</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>Tahun lulus*</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>IPK*</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>IPK ujian negara (jika ada)</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label>Status perguruan tinggi*</label>
			<input type="text" class="form-control">
		</div>	
		<div class="form-group col-lg-4">
			<label>Gelar</label>
			<input type="text" class="form-control">
		</div>	
	</div>
	<br>
	<br>
	<b>F. KEGIATAN LAIN</b>
	<br><br>
	<p>Penelitian dan publikasi 5 tahun terakhir</p>
	<br>
	<p>
		Penelitian <br>
		Cantumkan : Judul penelitian, Tahun, Jabatan dalam penelitian (ketua atau anggota), Sumber dana penelitian
	</p>
	<div class="row">
		<div class="form-group col-lg-12">
			<label>Alamat lengkap perguruan tinggi</label>
			<textarea style="height:30vh" class="form-control"></textarea>
		</div>
	</div>
	<br>
	<p>
		Publikasi ilmuah <br>
		Cantumkan : Pengarang, Tahun penerbitan, Judul, dimana dipublikasikan
	</p>
	<div class="row">
		<div class="form-group col-lg-12">
			<label>Alamat lengkap perguruan tinggi</label>
			<textarea style="height:30vh" class="form-control"></textarea>
		</div>
	</div>
	<label>
		<input type="checkbox"> Dengan ini saya menyatakan bahwa data diatas telah diisi dengan sebenar-benarnya.
	</label>
	<div class="row">
		<div class="col-lg-12">
			captcha
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<input type="submit" value="Daftar" class="btn btn-info">
		</div>
	</div>
</form>
<br>
<br>
<br>
<br>